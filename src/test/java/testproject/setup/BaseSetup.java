package testproject.setup;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import testproject.util.GetPropertiesFile;

public class BaseSetup {
	
	public WebDriver driver;
	
	public static String browser;
	public static String chromeBrowserDriverLocation;
	public static String firefoxBrowserDriverLocation;
	
	/**
	 * Get the webDriver instance
	 */
	public WebDriver getDriver() {
		return driver;
	}
	
	/**
	 * Creates the WebDriver based on the browser supplied
	 * @param browser - the browser type for which WebDriver to create
	 * @return - the WebDriver Instance
	 */
	public WebDriver createWebDriver(String browser){
		chromeBrowserDriverLocation = GetPropertiesFile.getProperty("chromeBrowserDriverLocation");
		firefoxBrowserDriverLocation = GetPropertiesFile.getProperty("firefoxBrowserDriverLocation");
		
		try{
			if(browser.equalsIgnoreCase("Chrome")){
				System.setProperty("webdriver.chrome.driver", chromeBrowserDriverLocation);
				driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			}else if (browser.equalsIgnoreCase("Firefox")){
				System.setProperty("webdriver.gecko.driver", firefoxBrowserDriverLocation);
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			}else{
				System.out.println("WebDriverManager - Browser Not Supported");
				driver = null;
			}
		}catch(Exception e) {
			System.err.println("WebDriverManager - Error creating WebDriver");
			e.printStackTrace();
		}
		
		return driver;
		
	}
	

	
	
}
