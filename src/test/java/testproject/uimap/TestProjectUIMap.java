package testproject.uimap;

import org.openqa.selenium.By;

public class TestProjectUIMap {
		
	    public static By	home_page_login			= By.cssSelector("[class='global-login__registration']"); 			// Login button on Home Page
	    public static By	login_page_continue 	= By.cssSelector("[id='continuebutton']");							// Continue button on Login Page
	    public static By    login_page_userName		= By.cssSelector("[id='username']");								// Username Inout Field on Login Page
	    public static By    login_page_password		= By.cssSelector("[id='password']");								// Password Input Field on Login Page
	    public static By    login_page_signIn 		= By.cssSelector("[id='signinbutton']");							// Sign in on Login Page
	    public static By	login_page_accountName	= By.cssSelector("[class='bx--account-switcher__toggle--text']");	// Account Name Details
}
