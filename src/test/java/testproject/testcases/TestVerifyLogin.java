package testproject.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import testproject.setup.BaseSetup;
import testproject.tasks.commonTasks;
import testproject.uimap.TestProjectUIMap;
import testproject.util.GetPropertiesFile;


public class TestVerifyLogin extends BaseSetup {		
	
	private static String url;
	private static String accname;
	private static String username;
	private static String password;
	public commonTasks tasks;
	
	public TestVerifyLogin() {
		
		browser = GetPropertiesFile.getProperty("browser");
		url = GetPropertiesFile.getProperty("url");
		accname = GetPropertiesFile.getProperty("accname");
		username = GetPropertiesFile.getProperty("username");
		password = GetPropertiesFile.getProperty("password");
		
		
	}
	

	/*
	 * Open the Browser
	 * 
	 */
	@BeforeTest
	public void openBrowser() {
		
		try{
			driver = createWebDriver(browser);
			driver.get(url);
			System.out.print("Successfully loaded the "+ browser + " WebDriver Instance");
			System.out.println();
		}catch (Exception e) {
			System.err.println("Error finding HomePage Login");
			e.printStackTrace();
		}
	
	}
	
	

	/*
	 * Login Method
	 *    
	 */
	@Test
	public void login() {
		String accnameEnding = "'s Account";
		
		try{driver.findElement(TestProjectUIMap.home_page_login).click();}catch (Exception e) {
			System.err.println("Error finding HomePage Login");
			e.printStackTrace();
		}
		try{driver.findElement(TestProjectUIMap.login_page_userName).sendKeys(username);}catch (Exception e) {
			System.err.println("Error finding Login Page Username Inout Field");
			e.printStackTrace();
		}
		try{driver.findElement(TestProjectUIMap.login_page_continue).click();}catch (Exception e) {
			System.err.println("Error finding Login Page Continue Button");
			e.printStackTrace();
		}
		try{driver.findElement(TestProjectUIMap.login_page_password).sendKeys(password);}catch (Exception e) {
			System.err.println("Error finding Login Page Password Input Field");
			e.printStackTrace();
		}
		try{driver.findElement(TestProjectUIMap.login_page_signIn).click();}catch (Exception e) {
			System.err.println("Error finding Login Page Sign-in Button");
			e.printStackTrace();
		}
		
		try{driver.findElement(TestProjectUIMap.login_page_accountName).getText().equalsIgnoreCase(accname+accnameEnding);}catch (Exception e) {
			System.err.println("Error verifying correct user is logged in");
			e.printStackTrace();
		}
		
		System.out.print("Successfully logged in to "+ driver.getTitle() + " as user " + accname);
		System.out.println();
		
		
	}
	
	
	/*
	 * Close the Web Browser
	 * 
	 */
	@AfterTest
	public void closeBrowser() {
		driver.close();
	}
	
	
}


