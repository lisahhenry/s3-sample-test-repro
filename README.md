## # README # ##

### This README documents the sample test for s3 demonstrating: ###
1. Create a single test in his preferred language e.g. C#/Java/Ruby
2. Framework Selenium
3. Test should search something on google, log in to something
4. The timeframe to produce the test should be in 3 working days

### **To run** ###
1. Download the source code.
2. Open Eclipse (or your preferred Java IDE) and import the downloaded source code.
3. Install Java 1.7 (if not already installed) and add to the project build path. Please compile against Java 1.7 also.
4. Run the pom.xml (as a maven test).
Maven will take care of all other dependencies and your test should execute.


### **Please Note** ###
* Supported browsers are Chrome & Firefox.
To slect the browser and to view/edit additional properties, please see
/selenium-java/src/main/java/resources/testprojectProperties.properties
* There are some additional refactoring/improvements that could be made to the code, especially as the project gets larger, but I hope this demonstrates the core capabilities. 



Please contact lisahhenry@gmail.com for any feedback/issues.